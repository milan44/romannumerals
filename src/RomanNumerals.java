//import java.util.HashMap;
import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RomanNumerals {
    /*private static final Pattern NUMERAL_REGEX = Pattern.compile("I[VX]?|V|X[LC]?|C[DM]?|M", Pattern.CASE_INSENSITIVE);
    private static final HashMap<String, Integer> NUMERAL_DEFINITION = new HashMap<>() {{
        put("I", 1);
        put("IV", 4);
        put("V", 5);
        put("IX", 9);
        put("X", 10);
        put("XL", 40);
        put("L", 50);
        put("XC", 90);
        put("C", 100);
        put("CD", 400);
        put("D", 500);
        put("CM", 900);
        put("M", 1000);
    }};*/

    public static int convertFromRomanToArabic(String roman) {
        //return convertFromRomanToArabic(roman, 0);
        return convertFromRomanToArabicInJava9(roman);
    }

    /*private static int convertFromRomanToArabic(String roman, int result) {
        Matcher matcher = NUMERAL_REGEX.matcher(roman);
        while (matcher.find()) {
            result += (int) NUMERAL_DEFINITION.get(matcher.group());
        }
        return result;
    }*/

    private static int convertFromRomanToArabicInJava9(String roman) {
        return Pattern.compile("I[VX]?|V|X[LC]?|C[DM]?|M", Pattern.CASE_INSENSITIVE).matcher(roman).results().map(MatchResult::group).map(Map.ofEntries(Map.entry("I", 1), Map.entry("IV", 4), Map.entry("V", 5), Map.entry("IX", 9), Map.entry("X", 10), Map.entry("XL", 40), Map.entry("L", 50), Map.entry("XC", 90), Map.entry("C", 100), Map.entry("CD", 400), Map.entry("D", 500), Map.entry("CM", 900), Map.entry("M", 1000))::get).mapToInt(i -> (int) i).sum();
    }
}