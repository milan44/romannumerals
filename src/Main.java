public class Main {
    public static void main(String[] args) {
        testNumeralConversion("I", 1);
        testNumeralConversion("II", 2);
        testNumeralConversion("IV", 4);
        testNumeralConversion("V", 5);
        testNumeralConversion("IX", 9);
        testNumeralConversion("XLII", 42);
        testNumeralConversion("XCIX", 99);
        testNumeralConversion("MMXIII", 2013);
    }

    private static void testNumeralConversion(String input, int expectedOutput) {
        int actualOutput = RomanNumerals.convertFromRomanToArabic(input);

        if (actualOutput == expectedOutput) {
            System.out.println(input + " === " + expectedOutput);
        } else {
            System.out.println(input + " !== " + expectedOutput + " (" + actualOutput + ")");
        }
    }
}
